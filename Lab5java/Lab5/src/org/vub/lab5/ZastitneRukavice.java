package org.vub.lab5;

public class ZastitneRukavice {
    public String boja;
    public int kolicina;

    public ZastitneRukavice(String boja, int kolicina){
        this.boja=boja;
        this.kolicina=kolicina;
    }

    public void ispis() {
        System.out.println("Zastitne rukavice: " + kolicina + ", " + boja);
    }
}
